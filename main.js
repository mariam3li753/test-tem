
$("#main-carousel").owlCarousel({
  loop: true,
  // margin: 10,
  nav: true,
  items: 1,
  autoplay: true,
  autoPlaySpeed: 3000,
});

$("#client-carousel").owlCarousel({
  loop: true,
  // margin: 10,
  nav: false,
  // items: 5,
  dots: false,
  autoplay: true,
  autoPlaySpeed: 2000,
  autoplayHoverPause: true,
  responsive: {
    0: {
      items: 2,
    },
    600: {
      items: 3,
    },
    1000: {
      items: 5,
    },
  },
});

$(document).ready(function () {
  $("ul.navbar-nav > li > a").click(function (e) {
    $("ul.navbar-nav > li > a").removeClass("active");
    $(this).addClass("active");
  });
});

$("#video-clicked").click(function() {
  $("#frame").addClass('clicked');
});

// $(document).ready(function(){
//     $(window).scroll(function(){
//         if($(document).scrollTop() > 500 ){
//             $("h2").addClass("fixed")
//         }
//         else{
//             $("h2").removeClass("fixed")
//         }
//     })
// });